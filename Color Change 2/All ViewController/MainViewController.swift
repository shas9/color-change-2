//
//  MainViewController.swift
//  Color Change 2
//
//  Created by Kite Games Studio on 11/8/21.
//

import UIKit

class MainViewController: UIViewController {
    
    var colorType: Colortype?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func redButtonPressed(_ sender: Any) {
        
        let StoryBoard = UIStoryboard.init(name: "Main", bundle: nil)
        
        if let colorVC = StoryBoard.instantiateViewController(identifier: "ColorableViewController") as? ColorableViewController {
            
            colorVC.colorableVCDelegate = self
            colorVC.colorType = .rose
            self.present(colorVC, animated: true, completion: nil)
        }
        
    }
    
    @IBAction func greenButtonPressed(_ sender: Any) {
    
        let StoryBoard = UIStoryboard.init(name: "Main", bundle: nil)
        
        if let colorVC = StoryBoard.instantiateViewController(identifier: "ColorableViewController") as? ColorableViewController {
            
            colorVC.colorableVCDelegate = self
            colorVC.colorType = .greenleaf
            self.present(colorVC, animated: true, completion: nil)
        }
        
    }
    
    @IBAction func blueButtonPressed(_ sender: Any) {
        
        let StoryBoard = UIStoryboard.init(name: "Main", bundle: nil)
        
        if let colorVC = StoryBoard.instantiateViewController(identifier: "ColorableViewController") as? ColorableViewController {
            
            colorVC.colorableVCDelegate = self
            colorVC.colorType = .oceanblue
            self.present(colorVC, animated: true, completion: nil)
        }

    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension MainViewController: ColorableViewControllerDelegate {
    
    // MARK: - Delegate
    func askColor(colorName: Colortype) -> UIColor {
        
        if colorName == .rose { return UIColor.red }
        else if colorName == .oceanblue { return UIColor.blue }
        else if colorName == .greenleaf { return UIColor.green }
        else { fatalError("This should not be here!") }
    }
    
    
}
