//
//  ColorableViewController.swift
//  Color Change 2
//
//  Created by Kite Games Studio on 11/8/21.
//

import UIKit

protocol ColorableViewControllerDelegate: NSObject {
    
    func askColor(colorName: Colortype) -> UIColor
    
}

class ColorableViewController: UIViewController {

    weak var colorableVCDelegate: ColorableViewControllerDelegate!
    var colorType: Colortype = .none
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func colorablebuttonpressed (_ sender: UIButton) {
        
        let newBackgroundColor = self.colorableVCDelegate.askColor(colorName: colorType)
        self.view.backgroundColor = newBackgroundColor
        
        sender.setTitle("Huh! Changed!", for: .normal)
        sender.backgroundColor = UIColor.lightGray
    }
    
    @IBAction func goBackButtonPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
