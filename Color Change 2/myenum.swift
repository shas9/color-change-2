//
//  myenum.swift
//  Color Change 2
//
//  Created by Kite Games Studio on 11/8/21.
//

import Foundation

enum Colortype {
    case none;
    case rose;
    case oceanblue;
    case greenleaf;
    
}
